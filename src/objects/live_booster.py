from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QTransform, QPen, QColor

from objects.types.booster import Booster


class LiveBooster(Booster):

    def draw(self, painter: QPainter) -> None:

        transform = QTransform()
        transform.translate(self.x, self.y)
        painter.setTransform(transform)

        painter.setPen(QPen(QColor(Qt.darkCyan), 3))

        painter.drawLine(self.width * 0.5, 0, self.width * 0.1, self.height)
        painter.drawLine(self.width * 0.5, 0, self.width * 0.9, self.height)
        painter.drawLine(self.width * 0.2, self.height * 0.8, self.width * 0.8, self.height * 0.8)

        painter.setTransform(QTransform())

    def activate(self):
        self.notify_observers(event="live booster has been activated")
