from random import choice

from PyQt5.QtCore import QPoint
from PyQt5.QtGui import QPainter

from objects.immortality_booster import ImmortalityBooster
from objects.live_booster import LiveBooster
from objects.types.booster import Booster


class BoosterManager:

    def __init__(self, game):
        self._boosters: list[Booster] = list()
        self._game = game

    @property
    def boosters(self) -> list[Booster]:
        return self._boosters

    def draw_boosters(self, painter: QPainter) -> None:
        for booster in self._boosters:
            booster.draw(painter)

    def generate_booster(self, x: int, y: int):
        booster = choice([LiveBooster, ImmortalityBooster])(QPoint(x, y))
        self.boosters.append(booster)
        booster.add_observer(self._game)

    def activate_booster(self, booster: Booster):
        booster.activate()
        self._boosters.remove(booster)
