from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtGui import QPainter, QTransform, QPen, QColor

from objects.types.booster import Booster


class ImmortalityBooster(Booster):

    def draw(self, painter: QPainter) -> None:

        transform = QTransform()
        transform.translate(self.x, self.y)
        painter.setTransform(transform)

        painter.setPen(QPen(QColor(Qt.cyan), 3))

        painter.drawPolygon([QPoint(self.width * 0.2, 0), QPoint(self.width * 0.8, 0),
                             QPoint(self.width, self.height * 0.3), QPoint(0, self.height * 0.3)])
        painter.drawPolygon([QPoint(self.width, self.height * 0.3), QPoint(0, self.height * 0.3),
                             QPoint(self.width * 0.5, self.height * 0.9)])
        painter.drawPolygon([QPoint(self.width * 0.3, self.height * 0.3), QPoint(self.width * 0.7, self.height * 0.3),
                             QPoint(self.width * 0.5, self.height * 0.9)])
        painter.drawPolygon([QPoint(self.width * 0.3, self.height * 0.3), QPoint(self.width * 0.7, self.height * 0.3),
                             QPoint(self.width * 0.5, 0)])

        painter.setTransform(QTransform())

    def activate(self):
        self.notify_observers(event="immortality booster has been activated")
