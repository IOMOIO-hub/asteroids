from random import randint

from PyQt5.QtGui import QPainter

from core.utils.observer.observable import Observable
from objects.ufo import UFO


class UFOManager(Observable):
    UFO_SHOT = "this ufo just shot"

    def __init__(self, game):
        super().__init__()
        self._UFOs: list[UFO] = []
        self.add_observer(game)

    @property
    def ufos(self) -> list[UFO]:
        return self._UFOs

    def generate_ufo(self, rank: int):
        self._UFOs.append(UFO(rank))

    def update(self) -> None:
        for ufo in self._UFOs:
            ufo.update()
            if ufo.shoot_counter == ufo.shoot_limit:
                self.notify_observers(event=self.UFO_SHOT, ufo=ufo)
                ufo.shoot_counter = 0
                ufo.shoot_limit = randint(120, 180)

    def draw_ufos(self, painter: QPainter) -> None:
        for ufo in self._UFOs:
            ufo.draw(painter)

    def remove_ufo(self, ufo: UFO):
        self._UFOs.remove(ufo)
