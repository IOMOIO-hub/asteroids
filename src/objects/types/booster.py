from abc import ABC, abstractmethod

from PyQt5.QtCore import QPoint, QSize

from core.utils.observer.observable import Observable
from objects.types.object import Object


class Booster(Object, Observable, ABC):
    def __init__(self, location: QPoint):
        Object.__init__(self, location, QSize(40, 40))
        Observable.__init__(self)

    def update(self):
        pass

    @abstractmethod
    def activate(self):
        raise NotImplementedError
