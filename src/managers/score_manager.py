from prettytable import PrettyTable

from core.utils.assets import Assets
from managers.types.score import Score, ScoreFactory

SCORES_FILE = Assets.SCORES_PATH
MAX_SCORE_SAVE = 10


class ScoreManager:
    def __init__(self) -> None:
        self._scores: list[Score] = list()
        with open(SCORES_FILE, "r") as file:
            for line in [row for row in file.readlines() if row]:
                line = line.strip("\n")
                score = ScoreFactory.from_str(line)
                self._scores.append(score)

    def register_new_score(self, score: int) -> None:
        if len(self._scores) < MAX_SCORE_SAVE:
            self._scores.append(Score(score))
            return self._dump_scores()

        least_score_i = 0
        for i in range(len(self._scores)):
            cur_score = self._scores[i].score
            least_score = self._scores[least_score_i].score
            least_score_i = i if least_score > cur_score else least_score_i

        if self._scores[least_score_i].score < score:
            self._scores[least_score_i] = Score(score)
            return self._dump_scores()

    def pretty_print(self) -> str:
        table = PrettyTable()
        table.field_names = ["Name", "Score", "Date"]

        for score in self._scores:
            table.add_row([score.name, score.score, score.date])
        return table.get_string(sortby="Score", reversesort=True).replace(" ", "  ")

    def _dump_scores(self) -> None:
        with open(SCORES_FILE, "w") as file:
            file.writelines([score.to_str() + "\n" for score in self._scores])
