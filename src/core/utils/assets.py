import sys
from os import path


def resource_path(relative: str) -> str:
    if hasattr(sys, "_MEIPASS"):
        return path.join(sys._MEIPASS, relative)
    return path.join(relative)


class Assets:
    _BASE_PATH = "assets"

    CONFIG_PATH = resource_path("config.yml")

    LOGO_PATH = resource_path(path.join(_BASE_PATH, "logo.png"))
    FONT_PATH = resource_path(path.join(_BASE_PATH, "speculum.ttf"))

    SCORES_PATH = resource_path("scores.txt")
