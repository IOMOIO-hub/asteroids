from PyQt5.QtWidgets import QWidget


class IScene(QWidget):

    def __init__(self):
        super().__init__()

    def key_pressed(self, key) -> None:
        pass

    def key_released(self, key) -> None:
        pass
