from yaml import safe_load

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from core.utils.assets import Assets
from core.utils.observer.observable import Observable
from managers.score_manager import ScoreManager
from scenes.types.iscene import IScene

from managers.asteroid_manager import AsteroidManager


class ScoreboardScene(IScene, Observable):
    TO_MENU_SCENE = 'scene has been switched to the menu'

    def __init__(self):
        IScene.__init__(self)
        Observable.__init__(self)

        self._score_manager = ScoreManager()

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        font_id = QFontDatabase.addApplicationFont(Assets.FONT_PATH)
        font_family = QFontDatabase.applicationFontFamilies(font_id)[0]
        custom_font = QFont(font_family, 32)

        self.scoreboard = QLabel(self._score_manager.pretty_print().replace('|', ''))

        self.scoreboard.setStyleSheet(
            "background-color: transparent; color: #ffffff; font-size: 20px")
        self.scoreboard.setFont(custom_font)
        self.scoreboard.setAlignment(Qt.AlignCenter)
        self.scoreboard.setFixedHeight(400)
        self.setup_layout()
        self.setup_decorative_asteroids()

    def setup_layout(self):
        vbox = QVBoxLayout()
        vbox.setAlignment(Qt.AlignCenter)

        button_style = '''
            background-color: transparent;
            color: white;
            padding: 10px;
        '''
        font_id = QFontDatabase.addApplicationFont(Assets.FONT_PATH)
        font_family = QFontDatabase.applicationFontFamilies(font_id)[0]
        custom_font = QFont(font_family, 32)

        logo = QLabel(self)
        pixmap = QPixmap(Assets.LOGO_PATH)
        logo.setPixmap(pixmap)
        logo.setAlignment(Qt.AlignCenter)
        logo.setFixedHeight(170)

        spacer = QSpacerItem(0, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)

        back_to_menu_button = QPushButton('BACK TO MENU', self)
        back_to_menu_button.setStyleSheet(button_style)
        back_to_menu_button.setFont(custom_font)
        back_to_menu_button.clicked.connect(
            lambda: self.notify_observers(event=self.TO_MENU_SCENE)
        )
        vbox.addWidget(logo)
        vbox.addWidget(self.scoreboard)
        vbox.addItem(spacer)
        vbox.addWidget(back_to_menu_button)

        self.setLayout(vbox)

    def setup_decorative_asteroids(self):
        self._timer = QTimer(self)
        self._asteroid_manager = AsteroidManager()
        self._asteroid_manager.generate_asteroids(6)
        self._timer.timeout.connect(self.update_and_draw_decorative_asteroids)
        self._timer.start(1000 // self._config['frame_rate'])

    def update_and_draw_decorative_asteroids(self):
        self._asteroid_manager.update()
        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.fillRect(0, 0, self._config['window_width'], self._config['window_height'], QBrush(Qt.SolidPattern))
        self._asteroid_manager.draw_asteroids(painter)
