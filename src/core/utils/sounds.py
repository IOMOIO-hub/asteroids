import sys
from os import path
import contextlib

with contextlib.redirect_stdout(None):
    from pygame import mixer


def resource_path(relative: str) -> str:
    if hasattr(sys, "_MEIPASS"):
        return path.join(sys._MEIPASS, relative)
    return path.join(relative)


class Sounds:
    mixer.init()

    _BASE_PATH = path.join("assets", "sounds")

    _BEAT1_PATH = path.join(_BASE_PATH, "beat1.wav")
    BEAT1 = mixer.Sound(resource_path(_BEAT1_PATH))

    _BEAT2_PATH = path.join(_BASE_PATH, "beat2.wav")
    BEAT2 = mixer.Sound(resource_path(_BEAT2_PATH))

    _BANG_SMALL_PATH = path.join(_BASE_PATH, "bangSmall.wav")
    BANG_SMALL = mixer.Sound(resource_path(_BANG_SMALL_PATH))

    _BANG_MEDIUM_PATH = path.join(_BASE_PATH, "bangMedium.wav")
    BANG_MEDIUM = mixer.Sound(resource_path(_BANG_MEDIUM_PATH))

    _BANG_LARGE_PATH = path.join(_BASE_PATH, "bangLarge.wav")
    BANG_LARGE = mixer.Sound(resource_path(_BANG_LARGE_PATH))

    _FIRE_PATH = path.join(_BASE_PATH, "fire.wav")
    FIRE = mixer.Sound(resource_path(_FIRE_PATH))

    _UFO_BIG_PATH = path.join(_BASE_PATH, "saucerBig.wav")
    UFO_BIG = mixer.Sound(resource_path(_UFO_BIG_PATH))

    _UFO_SMALL_PATH = path.join(_BASE_PATH, "saucerSmall.wav")
    UFO_SMALL = mixer.Sound(resource_path(_UFO_SMALL_PATH))

    _THRUST_PATH = path.join(_BASE_PATH, "thrust.wav")
    THRUST = mixer.Sound(resource_path(_THRUST_PATH))

    _EXTRA_SHIP_PATH = path.join(_BASE_PATH, "extraShip.wav")
    EXTRA_SHIP = mixer.Sound(resource_path(_EXTRA_SHIP_PATH))
