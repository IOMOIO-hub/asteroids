from math import sin, cos, pi

from PyQt5.QtCore import QPoint
from PyQt5.QtGui import QPainter
from yaml import safe_load

from core.utils.assets import Assets
from core.utils.sounds import Sounds
from objects.bullet import Bullet


class BulletManager:
    def __init__(self):
        self._bullets: list[Bullet] = list()

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

    @property
    def bullets(self) -> list[Bullet]:
        return self._bullets

    def update(self) -> None:
        for bullet in self._bullets:
            bullet.update()
            if bullet.is_expired():
                self.remove_bullet(bullet)

    def draw_bullets(self, painter: QPainter) -> None:
        for bullet in self._bullets:
            bullet.draw(painter)

    def remove_bullet(self, bullet: Bullet):
        self._bullets.remove(bullet)

    def shoot(self, entity, degree: int = None):

        if len(self._bullets) > self._config['max_amount_of_bullets']:
            return

        Sounds.FIRE.play()

        if not degree:
            degree = entity.degree

        new_bullet_x = entity.x + (entity.width // 2) + sin(pi / 180 * degree) * (entity.width // 2)
        new_bullet_y = entity.y + (entity.height // 2) - cos(pi / 180 * degree) * (entity.height // 2)

        self._bullets.append(Bullet(QPoint(new_bullet_x, new_bullet_y), degree, entity))
