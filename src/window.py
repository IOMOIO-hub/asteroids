from yaml import safe_load

from PyQt5.QtWidgets import QMainWindow

from core.utils.assets import Assets
from managers.scene_manager import SceneManager


class Window(QMainWindow):

    def __init__(self):
        super().__init__()

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        self.resize(self._config['window_width'], self._config['window_height'])
        self.setWindowTitle("Asteroids")

        self.scene_manager = SceneManager(self)

    def keyPressEvent(self, event):
        self.scene_manager._scene.key_pressed(event.key())

    def keyReleaseEvent(self, event):
        self.scene_manager._scene.key_released(event.key())
