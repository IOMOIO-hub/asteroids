from yaml import safe_load

from math import sin, cos, pi
from random import random

from PyQt5.QtGui import *
from PyQt5.QtCore import *

from core.utils.assets import Assets
from objects.types.object import Object


class Spaceship(Object):

    def __init__(self):

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        super().__init__(
            QPoint((self._config['window_width'] - 60) // 2,
                   (self._config['window_height'] - 45) // 2),
            QSize(45, 60)
        )

        self._speed_vectors = {}
        self._acceleration = self._config['spaceship_acceleration']
        self._deceleration = self._config['spaceship_deceleration']
        self._max_speed = self._config['spaceship_max_speed']

        self._immortal_time_counter = self._config['spaceship_immortal_time']

        self.boosting = False
        self.shooting = False
        self.rotation_factor = 0

    def boost_speed(self):
        self._speed_vectors[self._degree] = (
            min(self._speed_vectors.get(self._degree, 0) + self._acceleration, self._max_speed)
        )

    def reset(self):
        self.x = (self._config['window_width'] - self.width) // 2
        self.y = (self._config['window_height'] - self.height) // 2
        self._speed_vectors = {}
        self._degree = 0
        self._immortal_time_counter = self._config['spaceship_immortal_time']

    def update(self):
        self._immortal_time_counter -= 1

        if self.boosting: self.boost_speed()
        if self.rotation_factor: self._degree += self.rotation_factor * 7.5

        keys_to_remove = []
        for degree in self._speed_vectors:
            speed = self._speed_vectors[degree]
            self.x += speed * sin(pi / 180 * degree)
            self.y -= speed * cos(pi / 180 * degree)

            self._speed_vectors[degree] -= self._deceleration
            if self._speed_vectors[degree] < 0:
                keys_to_remove.append(degree)

        for key in keys_to_remove:
            del self._speed_vectors[key]

        self.through_borders()

    def draw(self, painter: QPainter):

        transform = QTransform()
        transform.translate(self.x, self.y)
        transform.translate(self.width * 0.5, self.height * 0.5)
        transform.rotate(self._degree)
        transform.translate(-self.width * 0.5, -self.height * 0.5)
        painter.setTransform(transform)

        if self._immortal_time_counter > 0:
            painter.setPen(QPen(QColor(Qt.lightGray), 2, Qt.DashLine))
        else:
            painter.setPen(QPen(QColor(Qt.white), 3))

        painter.drawLine(self.width * 0.5, 0, 0, self.height)
        painter.drawLine(self.width * 0.5, 0, self.width, self.height)
        painter.drawLine(self.width * 0.1, self.height * 0.8, self.width * 0.9, self.height * 0.8)

        if self.boosting and random() < 0.5:
            painter.drawLine(self.width * 0.5, self.height + 10, self.width * 0.3, self.height * 0.8)
            painter.drawLine(self.width * 0.5, self.height + 10, self.width * 0.7, self.height * 0.8)

        painter.setTransform(QTransform())

    @property
    def degree(self):
        return self._degree

    @property
    def immortal_time_counter(self):
        return self._immortal_time_counter
