from random import randint

from yaml import safe_load
from math import sin, cos, pi, atan

from PyQt5.QtGui import *
from PyQt5.QtCore import *

from core.utils.assets import Assets
from objects.types.object import Object


class UFO(Object):
    def __init__(self, rank: int):

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        position = QPoint(randint(0, self._config['window_width']), -60)
        size = {1: QSize(60, 45), 2: QSize(80, 60)}[rank]
        speed = {1: 4, 2: 2}[rank]
        degree = randint(0, 11) * 30
        super().__init__(position, size, speed, degree)

        self.rank = rank

        self.change_degree_counter = 0
        self.change_degree_limit = randint(120, 180)

        self.shoot_counter = 0
        self.shoot_limit = randint(120, 180)

    def update(self):

        self.shoot_counter += 1

        self.change_degree_counter += 1
        if self.change_degree_counter == self.change_degree_limit:
            self._degree = randint(0, 11) * 30
            self.change_degree_counter = 0
            self.change_degree_limit = randint(120, 180)

        self.x += self._speed * sin(pi / 180 * self._degree)
        self.y -= self._speed * cos(pi / 180 * self._degree)

        self.through_borders()

    def draw(self, painter: QPainter):
        transform = QTransform()
        transform.translate(self.x, self.y)
        painter.setTransform(transform)

        painter.setPen(QPen(QColor(Qt.white), 3))

        painter.drawPolygon([QPoint(self.width * 0.4, self.height * 0.1), QPoint(self.width * 0.6, self.height * 0.1),
                             QPoint(self.width * 0.7, self.height * 0.4), QPoint(self.width * 0.3, self.height * 0.4)])
        painter.drawPolygon([QPoint(self.width * 0.3, self.height * 0.4), QPoint(self.width * 0.7, self.height * 0.4),
                             QPoint(self.width, self.height * 0.7), QPoint(0, self.height * 0.7)])
        painter.drawPolygon([QPoint(self.width * 0.3, self.height), QPoint(self.width * 0.7, self.height),
                             QPoint(self.width, self.height * 0.7), QPoint(0, self.height * 0.7)])

        painter.setTransform(QTransform())

    def get_shoot_degree(self, target_x, target_y) -> int:
        if self.rank == 2:
            return randint(0, 11) * 30
        temp_degree = int(atan((abs(target_x - self.x)) / (abs(target_y - self.y))) * (180 / pi))
        if target_x > self.x and target_y < self.y:
            return temp_degree
        elif target_x < self.x and target_y < self.y:
            return temp_degree - 90
        elif target_x < self.x and target_y > self.y:
            return temp_degree - 180
        else:
            return -temp_degree + 180
