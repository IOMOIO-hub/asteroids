from yaml import safe_load

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from core.utils.assets import Assets
from core.utils.observer.observable import Observable
from scenes.types.iscene import IScene

from managers.asteroid_manager import AsteroidManager


class MenuScene(IScene, Observable):
    TO_GAME_SCENE = 'scene has been switched to the game'
    TO_SCOREBOARD_SCENE = 'scene has been switched to the scoreboard'

    def __init__(self):
        IScene.__init__(self)
        Observable.__init__(self)

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        self.setup_layout()
        self.setup_decorative_asteroids()

    def setup_layout(self):
        vbox = QVBoxLayout()
        vbox.setAlignment(Qt.AlignCenter)

        button_style = '''
            background-color: transparent;
            color: white;
            padding: 10px;
        '''
        font_id = QFontDatabase.addApplicationFont(Assets.FONT_PATH)
        font_family = QFontDatabase.applicationFontFamilies(font_id)[0]
        custom_font = QFont(font_family, 32)

        logo = QLabel(self)
        pixmap = QPixmap(Assets.LOGO_PATH)
        logo.setPixmap(pixmap)
        logo.setAlignment(Qt.AlignCenter)
        logo.setFixedHeight(170)

        spacer = QSpacerItem(0, 150, QSizePolicy.Expanding, QSizePolicy.Minimum)

        play_button = QPushButton('PLAY GAME', self)
        play_button.setStyleSheet(button_style)
        play_button.setFont(custom_font)
        play_button.clicked.connect(
            lambda: self.notify_observers(event=self.TO_GAME_SCENE)
        )

        high_scores_button = QPushButton('HIGH SCORES', self)
        high_scores_button.setStyleSheet(button_style)
        high_scores_button.setFont(custom_font)
        high_scores_button.clicked.connect(
            lambda: self.notify_observers(event=self.TO_SCOREBOARD_SCENE)
        )

        vbox.addWidget(logo)
        vbox.addItem(spacer)
        vbox.addWidget(play_button)
        vbox.addWidget(high_scores_button)

        self.setLayout(vbox)

    def setup_decorative_asteroids(self):
        self._timer = QTimer(self)
        self._asteroid_manager = AsteroidManager()
        self._asteroid_manager.generate_asteroids(6)
        self._timer.timeout.connect(self.update_and_draw_decorative_asteroids)
        self._timer.start(1000 // self._config['frame_rate'])

    def update_and_draw_decorative_asteroids(self):
        self._asteroid_manager.update()
        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.fillRect(0, 0, self._config['window_width'], self._config['window_height'], QBrush(Qt.SolidPattern))
        self._asteroid_manager.draw_asteroids(painter)
