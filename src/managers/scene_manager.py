from PyQt5.QtWidgets import QMainWindow

from core.utils.observer.iobserver import IObserver

from scenes.types.iscene import IScene
from scenes.game_scene import GameScene
from scenes.menu_scene import MenuScene
from scenes.scoreboard_scene import ScoreboardScene


class SceneManager(IObserver):

    def __init__(self, main_window: QMainWindow):
        self._main_window = main_window

        self._scene = None
        self.change_scene(MenuScene())

        self._main_window.setCentralWidget(self._scene)

    @property
    def scene(self) -> IScene:
        return self._scene

    def change_scene(self, next_scene: IScene) -> None:
        self._scene = next_scene
        self._scene.add_observer(self)
        self._main_window.setCentralWidget(self._scene)

    def notice(self, *args, **kwargs) -> None:
        {
            MenuScene.TO_GAME_SCENE: lambda: self.change_scene(GameScene()),
            MenuScene.TO_SCOREBOARD_SCENE: lambda: self.change_scene(ScoreboardScene()),
            GameScene.TO_MENU_SCENE: lambda: self.change_scene(MenuScene()),
            ScoreboardScene.TO_MENU_SCENE: lambda: self.change_scene(MenuScene())
        }[kwargs.get('event')]()
