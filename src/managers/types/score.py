import datetime
from dataclasses import dataclass


@dataclass
class Score:
    score: int
    name: str = "user"
    date: datetime.date = datetime.date.today()

    def to_str(self) -> str:
        return f"{self.name},{self.score},{self.date}"


class ScoreFactory:
    @classmethod
    def from_str(cls, score: str) -> Score:
        name, score, date = score.split(",")
        return Score(int(score), name, datetime.date.fromisoformat(date))
