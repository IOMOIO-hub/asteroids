from yaml import safe_load

from PyQt5.QtCore import *
from PyQt5.QtGui import QPainter

from core.utils.assets import Assets
from core.utils.observer.observable import Observable
from game import Game
from scenes.types.iscene import IScene


class GameScene(IScene, Observable):
    TO_MENU_SCENE = 'scene has been switched to the menu'
    TO_GAME_SCENE = 'scene has been switched to the game'

    def __init__(self):
        super().__init__()

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        self._game = Game()

        self._timer = QTimer(self)
        self._timer.timeout.connect(self.game_loop)
        self.play()

    def play(self):
        self._timer.start(1000 // self._config['frame_rate'])

    def pause(self):
        self._timer.stop()

    def game_loop(self):
        self._game.update()
        self.update()

    def paintEvent(self, event):
        self._game.draw(QPainter(self))

    def key_pressed(self, key):
        keyboard = {
            Qt.Key_W: self._game.start_boosting,
            Qt.Key_D: self._game.start_rotation_to_right,
            Qt.Key_A: self._game.start_rotation_to_left,
            Qt.Key_Space: self._game.shoot,
            Qt.Key_Escape: self.pause if self._timer.isActive() else self.play,
            Qt.Key_R: lambda: self.notify_observers(event=self.TO_GAME_SCENE),
            Qt.Key_M: lambda: self.notify_observers(event=self.TO_MENU_SCENE)
        }
        if key in keyboard:
            keyboard[key]()

    def key_released(self, key):
        keyboard = {
            Qt.Key_W: self._game.stop_boosting,
            Qt.Key_D: self._game.stop_rotation,
            Qt.Key_A: self._game.stop_rotation
        }
        if key in keyboard:
            keyboard[key]()
