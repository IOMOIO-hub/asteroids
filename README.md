<p align="center">
  <img src="src/assets/logo.png">
</p>
<span style="font-family: Lucida Console;"> </span>
<p align="center"><span style="font-family: Lucida Console;">Классическая игра в стиле аркады, в которой игрок управляет космическим кораблем и должен маневрировать сквозь поле астероидов, пытаясь уничтожить все астероиды на своем пути.</span></p>

## <span style="font-family: Lucida Console;">Геймплей</span>

<span style="font-family: Lucida Console;">Корабль игрока может двигаться в любом направлении и стрелять пулями, чтобы уничтожать астероиды. Будьте осторожны, так как астероиды разбиваются на более мелкие куски при попадании. Игроки также столкнутся с враждебными космическими кораблями, которые будут пытаться их сбить.</span>
<span style="font-family: Lucida Console;">Цель игры - выжить как можно дольше и набрать как можно больше очков, уничтожая астероиды и вражеские корабли. Игра становится все сложнее по мере того, как на экране появляется все больше астероидов и врагов.</span>

<p align="center"><span style="font-family: Lucida Console;">Подбирайте бустеры, выпадающие из астероидов, чтобы облегчить уничтожение вражеских НЛО!</span></p>
<p align="center">
  <img src="readmefiles/boosters.gif">
</p>

## <span style="font-family: Lucida Console;">Управление</span>

- <span style="font-family: Lucida Console;">Используйте клавиши WASD на клавиатуре для движения вашего космического корабля</span>
- <span style="font-family: Lucida Console;">Нажмите Spacebar, чтобы стрелять пулями</span>
- <span style="font-family: Lucida Console;">Нажмите клавишу R, если хотите начать заново</span>
- <span style="font-family: Lucida Console;">Нажмите клавишу Escape, чтобы поставить игру на паузу, нажмите на Escape еще раз, чтобы продолжить играть</span>
- <span style="font-family: Lucida Console;">Нажмите клавишу M, если хотите выйти в меню</span>