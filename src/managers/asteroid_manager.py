from PyQt5.QtCore import QPoint
from PyQt5.QtGui import QPainter

from src.objects.asteroid import Asteroid


class AsteroidManager:
    def __init__(self):
        self._asteroids: list[Asteroid] = list()

    @property
    def asteroids(self) -> list[Asteroid]:
        return self._asteroids

    def update(self) -> None:
        for asteroid in self._asteroids:
            asteroid.update()

    def draw_asteroids(self, painter: QPainter) -> None:
        for asteroid in self._asteroids:
            asteroid.draw(painter)

    def generate_asteroids(self, n: int) -> None:
        for _ in range(n):
            self._asteroids.append(Asteroid())

    def destroy_asteroid(self, asteroid: Asteroid) -> None:
        self._asteroids.remove(asteroid)

        if asteroid.rank > 1:
            self._asteroids.append(Asteroid(asteroid.rank - 1, QPoint(asteroid.x, asteroid.y)))
            self._asteroids.append(Asteroid(asteroid.rank - 1, QPoint(asteroid.x, asteroid.y)))
