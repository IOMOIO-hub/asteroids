from random import random

from yaml import safe_load

from PyQt5.QtCore import *
from PyQt5.QtGui import *

from core.utils.assets import Assets
from core.utils.observer.iobserver import IObserver
from core.utils.observer.observable import Observable
from managers.booster_manager import BoosterManager

from src.managers.asteroid_manager import AsteroidManager
from managers.bullet_manager import BulletManager
from managers.ufo_manager import UFOManager
from src.managers.score_manager import ScoreManager

from objects.spaceship import Spaceship
from objects.ufo import UFO

from core.utils.sounds import Sounds


class Game(IObserver):

    def __init__(self):

        self._spaceship = Spaceship()

        self._asteroid_manager = AsteroidManager()
        self._bullet_manager = BulletManager()
        self._ufo_manager = UFOManager(self)
        self._booster_manager = BoosterManager(self)
        self._score_manager = ScoreManager()

        self._stage = 0
        self._score = 0
        self._lives = 3

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        self._rest_time_counter = 60 * self._config['rest_time']
        self._beat_counter = 0

    def update(self):
        self._spaceship.update()
        self._asteroid_manager.update()
        self._bullet_manager.update()
        self._ufo_manager.update()

        if not self._ufo_manager.ufos:
            self._rest_time_counter -= 1

        self._beat_counter += 1

        music_tempo = 60 if self._rest_time_counter > 300 else 30

        if self._beat_counter % music_tempo == 0:
            {0: Sounds.BEAT1, 1: Sounds.BEAT2}[int(self._beat_counter % (music_tempo * 2) == 0)].play()

        asteroids_to_destroy = set()
        bullets_to_remove = set()
        ufos_to_remove = set()
        boosters_to_activate = set()

        for ufo in self._ufo_manager.ufos:
            if self._beat_counter % (ufo.rank * 5) == 0:
                {1: Sounds.UFO_SMALL, 2: Sounds.UFO_BIG}[ufo.rank].play()
            if ufo.is_collide(self._spaceship):
                ufos_to_remove.add(ufo)
                self._lives -= 1
                self._spaceship.reset()

        for booster in self._booster_manager.boosters:
            if self._spaceship.is_collide(booster):
                boosters_to_activate.add(booster)

        for asteroid in self._asteroid_manager.asteroids:

            if self._spaceship.is_collide(asteroid):
                if self._spaceship.immortal_time_counter < 0:
                    asteroids_to_destroy.add(asteroid)
                    self._lives -= 1
                    self._spaceship.reset()
                else:
                    self._spaceship._immortal_time_counter += 1

            for ufo in self._ufo_manager.ufos:
                if ufo.is_collide(asteroid):
                    asteroids_to_destroy.add(asteroid)
                    ufos_to_remove.add(ufo)

        for bullet in self._bullet_manager.bullets:

            if isinstance(bullet.shooter, UFO) and bullet.is_collide(self._spaceship):
                if self._spaceship.immortal_time_counter < 0:
                    bullets_to_remove.add(bullet)
                    self._lives -= 1
                    self._spaceship.reset()

            for asteroid in self._asteroid_manager.asteroids:
                if bullet.is_collide(asteroid):
                    bullets_to_remove.add(bullet)
                    asteroids_to_destroy.add(asteroid)

            for ufo in self._ufo_manager.ufos:
                if bullet.shooter != ufo and bullet.is_collide(ufo):
                    bullets_to_remove.add(bullet)
                    ufos_to_remove.add(ufo)

        for asteroid in asteroids_to_destroy:
            if random() < 0.1:
                self._booster_manager.generate_booster(asteroid.x, asteroid.y)

            self._score += {1: 100, 2: 50, 3: 20}[asteroid.rank]
            {1: Sounds.BANG_SMALL, 2: Sounds.BANG_MEDIUM, 3: Sounds.BANG_LARGE}[asteroid.rank].play()
            self._asteroid_manager.destroy_asteroid(asteroid)

        for bullet in bullets_to_remove:
            self._bullet_manager.remove_bullet(bullet)

        for ufo in ufos_to_remove:
            self._score += {1: 1000, 2: 200}[ufo.rank]
            {1: Sounds.BANG_MEDIUM, 2: Sounds.BANG_LARGE}[ufo.rank].play()
            self._ufo_manager.remove_ufo(ufo)

        for booster in boosters_to_activate:
            Sounds.EXTRA_SHIP.play()
            self._booster_manager.activate_booster(booster)

        if not self._asteroid_manager.asteroids:
            self._stage += 1
            self._spaceship._immortal_time_counter = self._config['spaceship_immortal_time']
            self._asteroid_manager.generate_asteroids(self._stage + 3)

        if self._rest_time_counter < 0:
            self._ufo_manager.generate_ufo(2)
            if self._stage > 1:
                self._ufo_manager.generate_ufo(1)
            self._rest_time_counter = 60 * self._config['rest_time']

        if self._lives < 0:
            self._score_manager.register_new_score(self._score)
            self.__init__()

    def draw(self, painter: QPainter):
        painter.fillRect(0, 0, self._config['window_width'], self._config['window_height'], QBrush(Qt.SolidPattern))

        self._booster_manager.draw_boosters(painter)
        self._spaceship.draw(painter)
        self._asteroid_manager.draw_asteroids(painter)
        self._bullet_manager.draw_bullets(painter)
        self._ufo_manager.draw_ufos(painter)

        painter.setPen(QColor(Qt.white))
        painter.setFont(QFont('Courier New', 24))
        painter.drawText(20, 60, str(self._score))
        painter.drawText(20, 100, "A" * self._lives)

    def start_boosting(self):
        Sounds.THRUST.play()
        self._spaceship.boosting = True
        self.stop_rotation()

    def stop_boosting(self):
        self._spaceship.boosting = False

    def start_rotation_to_right(self):
        self._spaceship.rotation_factor = 1
        self.stop_boosting()

    def start_rotation_to_left(self):
        self._spaceship.rotation_factor = -1
        self.stop_boosting()

    def stop_rotation(self):
        self._spaceship.rotation_factor = 0

    def shoot(self):
        self._bullet_manager.shoot(self._spaceship)

    def notice(self, *args, **kwargs) -> None:
        if kwargs.get("event") == UFOManager.UFO_SHOT:
            ufo = kwargs.get("ufo")
            self._bullet_manager.shoot(ufo, ufo.get_shoot_degree(self._spaceship.x, self._spaceship.y))
        elif kwargs.get("event") == "immortality booster has been activated":
            self._spaceship._immortal_time_counter = 5 * 60
        elif kwargs.get("event") == "live booster has been activated":
            self._lives += 1
