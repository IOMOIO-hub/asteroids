from math import sin, cos, pi

from yaml import safe_load

from core.utils.assets import Assets
from objects.types.object import Object

from PyQt5.QtGui import *
from PyQt5.QtCore import *


class Bullet(Object):

    def __init__(self, location: QPoint, degree: int, shooter: Object):

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        super().__init__(location, QSize(8, 8), self._config['bullet_speed'], degree)

        self._shooter = shooter
        self._expiration_time_counter = self._config['bullet_expiration_time']

    def update(self):
        self.x += self._speed * sin(pi / 180 * self._degree)
        self.y -= self._speed * cos(pi / 180 * self._degree)
        self._expiration_time_counter -= 1
        self.through_borders()

    def draw(self, painter: QPainter):
        painter.setPen(QPen(QColor(Qt.white), 3))
        painter.translate(self.x, self.y)
        painter.drawRoundedRect(2, 2, self.width - 4, self.height - 4, 1, 1)
        painter.translate(-self.x, -self.y)

    def is_out_of_view(self):
        return not (0 < self.x < self._config['window_width'] and 0 < self.y < self._config['window_height'])

    def is_expired(self):
        return self._expiration_time_counter < 0

    @property
    def shooter(self):
        return self._shooter
